package org.hua.fof;

import java.util.*;
import java.util.regex.Pattern;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;


public class Fof {

    private static final Pattern SPACE = Pattern.compile(" ");

    public static JavaPairRDD<Long, Long> compute(JavaRDD<String> lines) {

        JavaPairRDD<Long, Long> pairRddFirst = lines.flatMapToPair((String s) -> {
            String x[] = SPACE.split(s);
            long source = Long.parseLong(x[0]);
            long target = Long.parseLong(x[1]);

            return Arrays.asList(new Tuple2<>(source, target), new Tuple2<>(target, source)).iterator();
        });
        pairRddFirst.saveAsTextFile("/home/vagrant/pairRddFirst");
       // pairRddFirst.cache();


        JavaPairRDD<Long, Iterable<Long>> adjacencyList = pairRddFirst.groupByKey();
        adjacencyList.saveAsTextFile("/home/vagrant/adjacencyList");
        adjacencyList.sortByKey();
// Dimioyrgia PairRDD me tin morfi (fileName, word)
        JavaPairRDD<Tuple2<Long, Long> ,Long> filePair = adjacencyList.flatMapToPair(s -> {

            List<Tuple2<Tuple2<Long,Long>, Long> >listFile = new ArrayList<>();

            Long node = s._1;
            for (long word : s._2()) {
                for (long word1 : s._2()) {

                    if (word!=word1) {
                        if (word1>word) {
                            listFile.add(new Tuple2<>(new Tuple2<>(word,word1), node));
                        }
                    }
                }
            }
            return listFile.iterator();
        });

       // filePair.saveAsTextFile("/home/vagrant/filePair");


        JavaPairRDD<Long, Integer> allWordsPair = pairRddFirst.mapToPair(s -> {

            return new Tuple2<>(s._2, 1);
        });

    allWordsPair.saveAsTextFile("/home/vagrant/allWordsPair");

        JavaPairRDD<Long, Integer> allDocumentWordsCountPair = allWordsPair.reduceByKey((a, b) -> {
            return a + b;
        });

        JavaPairRDD<Tuple2<Long, Long>, Long> flatFirst = pairRddFirst.mapToPair(s -> {

            return new Tuple2<>(new Tuple2<>(s._1, s._2), 0L);
        });

        JavaPairRDD<Tuple2<Long, Long>, Tuple2<Long,Long>> urlNeighborsAndRank = filePair.join(flatFirst);
        urlNeighborsAndRank.saveAsTextFile("/home/vagrant/urlNeighborsAndRank");

        JavaPairRDD<Long, Double> allnodes = urlNeighborsAndRank.mapToPair(s -> {

            return new Tuple2<>(s._2._1, 1.0);
        }).reduceByKey((a, b) -> {
            return a + b; });


    allDocumentWordsCountPair.saveAsTextFile("/home/vagrant/allDocumentWordsCountPair");

        JavaPairRDD<Long, Double> allDocumentPair = allDocumentWordsCountPair.mapToPair((s) -> {

            Double s2 = new Double(s._2);
            Long s1 = s._1;

            Double num = (s2 * (s2 - 1)) / 2;


            return new Tuple2<>(s1, num);
        });
      //  allDocumentPair.cache();

        allDocumentPair.saveAsTextFile("/home/vagrant/allDocumentPair");

        JavaPairRDD<Long, Tuple2<Double,Double>> allJoinrank = allnodes.join(allDocumentPair);

        allJoinrank.saveAsTextFile("/home/vagrant/allJoinrank");

        JavaPairRDD<Long, Double> nodeRank = allJoinrank.mapToPair(s->{

            Double sum = s._2._1 / s._2._2 ;

            return new Tuple2<>(s._1,sum);
        });
      //  nodeRank.cache();

        JavaPairRDD<Long, Iterable<Double>> rankList = nodeRank.groupByKey();

        //rankList.cache();
        Long sizeOfRank = adjacencyList.count();
        Double sizeOfRankD = Double.parseDouble(sizeOfRank.toString());

        JavaPairRDD<String, Double> rankListGroup = nodeRank.mapToPair(s -> {

            return new Tuple2<>("all_Nodes", s._2);
        }).reduceByKey((a, b) -> {
            return (a + b);
        }).mapToPair(s -> {

            Double finalCon = s._2/sizeOfRankD;
            return new Tuple2<>("all_Nodes/6", finalCon);
        });


        rankListGroup.saveAsTextFile("/home/vagrant/rankListGroup");

        nodeRank.saveAsTextFile("/home/vagrant/nodeRank");

        return pairRddFirst;
    }


    public static void main(String[] args) throws Exception {

        if (args.length < 2) {
            System.err.println("Usage: Fof <input-path> <output-path>");
            System.exit(1);
        }

        SparkConf sparkConf = new SparkConf().setAppName("Fof");
        JavaSparkContext sc = new JavaSparkContext(sparkConf);

        JavaRDD<String> lines = sc.textFile(args[0]);

        JavaPairRDD<Long, Long> fofEdges = compute(lines);

        fofEdges.saveAsTextFile(args[1]);

        sc.stop();

    }
}